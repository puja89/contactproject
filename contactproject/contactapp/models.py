from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=400, null=True, blank=True)
    mobile1 = models.CharField(max_length=10)
    mobile2 = models.CharField(max_length=10, null=True, blank=True)
    email1 = models.EmailField(null=True, blank=True)
    email2 = models.EmailField(null=True, blank=True)
    image = models.ImageField(upload_to="contacts", null=True, blank=True)

    def __str__(self):
        return self.name

