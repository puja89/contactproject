from django.urls import path
from .views import *

app_name = 'contactapp'
urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('contacts/', ContactListView.as_view(), name="contacts"),
    path('contacts/about', ContactAboutView.as_view(), name="contactabout"),
    path('contacts/search', ContactSearchView.as_view(), name="search"),
    path('contacts/add-new/',
         ContactCreateView.as_view(), name="contactcreate"),
    path('contacts/<int:pk>/update/',
         ContactUpdateView.as_view(), name="contactupdate"),
    path('contacts/<int:pk>/delete/',
         ContactDeleteView.as_view(), name="contactdelete"),

    path('contacts/<email>/send-message/',
         SendEmailView.as_view(), name="sendemail"),
]
