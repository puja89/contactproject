from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import *
from .models import *
from django.core.mail import send_mail
from django.conf import settings
from .forms import *
from django.db.models import Q
import requests


class HomeView(TemplateView):
    template_name = "home.html"


class ContactListView(TemplateView):
    template_name = "contacts.html"
    success_message = "messages was send successfully"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET:

            q = self.request.GET["w"]
            results = Contact.objects.filter(
                Q(name__contains=q) | Q(email1__contains=q) |
                Q(mobile1__contains=q) | Q(address__contains=q))
        else:
            results = Contact.objects.all()

        resp = requests.get("https://ratopati.com/jsonapi/get_recent_posts/")
        data = resp.json()
        context['allnews'] = data['posts']
        context['allcontacts'] = results

        return context
    # def form_valid(self, form):
    #     fnames = form.cleaned_data['name']
    #     messages = form.cleaned_data['message']

    #     send_mail(
    #         fnames + ", ",
    #         messages,

    #         settings.EMAIL_HOST_USER,
    #         ["aryalpooja89@gmail.com"],
    #         fail_silently=False

    #     )

    #     return super().form_valid(form)


class ContactCreateView(CreateView):
    template_name = "contactcreate.html"
    form_class = ContactForm
    success_url = reverse_lazy("contactapp:contacts")


class ContactUpdateView(UpdateView):
    template_name = "contactcreate.html"
    form_class = ContactForm
    model = Contact
    success_url = reverse_lazy("contactapp:contacts")


class ContactDeleteView(DeleteView):
    template_name = "contactdelete.html"
    model = Contact
    success_url = reverse_lazy("contactapp:contacts")


class SendEmailView(FormView):
    template_name = "contactreply.html"
    form_class = ReplyForm
    success_url = reverse_lazy("contactapp:contacts")

    def form_valid(self, form):
        sub = form.cleaned_data['subject']
        mess = form.cleaned_data['message']
        email = self.kwargs['email']
        print(sub, mess, email)

        send_mail(
            sub,
            mess,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False
        )
        return super().form_valid(form)


class ContactSearchView(TemplateView):
    template_name = "contactsearch.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        q = self.request.GET["w"]
        results = Contact.objects.filter(
            Q(name__contains=q) | Q(email1__contains=q) |
            Q(mobile1__contains=q) | Q(address__contains=q))
        context['results'] = results

        return context

class ContactAboutView(TemplateView):
    template_name="contactabout.html"